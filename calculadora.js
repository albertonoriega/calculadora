
const pantalla = document.querySelector('.display');

const numeros = [
    document.querySelector('#cero'),
    document.querySelector('#uno'),
    document.querySelector('#dos'),
    document.querySelector('#tres'),
    document.querySelector('#cuatro'),
    document.querySelector('#cinco'),
    document.querySelector('#seis'),
    document.querySelector('#siete'),
    document.querySelector('#ocho'),
    document.querySelector('#nueve'),
];

for (let i = 0; i < numeros.length; i++) {

    numeros[i].addEventListener('click', function () {
        pantalla.innerHTML += i;
    });
}

// operaciones 


let botonSuma = document.querySelector('#suma');
let botonResta = document.querySelector('#resta');
let botonProducto = document.querySelector('#producto');
let botonDivision = document.querySelector('#division');

botonSuma.addEventListener('click', function () {
    pantalla.innerHTML += "+";
});

botonResta.addEventListener('click', function () {
    pantalla.innerHTML += "-";
});

botonProducto.addEventListener('click', function () {
    pantalla.innerHTML += "*";
});

botonDivision.addEventListener('click', function () {
    pantalla.innerHTML += "/";
});


// reset 
let botonReset = document.querySelector('#reset');

botonReset.addEventListener('click', function () {
    // borramos todo el contenido de la pantalla
    pantalla.innerHTML = "";
});

//borrar 

let botonBorrar = document.querySelector('.borrar');

botonBorrar.addEventListener('click', function () {

    let texto = pantalla.innerHTML;
    // Elimamos el último cáracter de la pantalla
    texto = texto.substring(0, texto.length - 1);

    pantalla.innerHTML = texto;
});

// igual

let botonIgual = document.querySelector('#igual');

botonIgual.addEventListener('click', function () {
    let operacion = pantalla.innerHTML;


    pantalla.innerHTML = operacion;
});


